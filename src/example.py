"""
This is a small example program as a demonstration of the PGUI module!
It's based on pygame and I wrote it to prevent myself from constantly
rewriting the same code over and over.
If you're in an editor like VSCode you can hover over each class and
function for some more detail.
"""

from PGUI import Program, Window, loadify
import PGUI.UIElements as elements

from random import randint

"""Windows are controlled by programs. This one starts by running the window 
with the ID "example" and runs all of its windows at 30 frames per second."""
example_program = Program(default_window="example", fps=30)

"""Programs have colours that the elements and the windows can use.
The colours go from lightest to darkest, and the supplied elements use four."""
example_program.colours = [
    ##Colours are stored as tuples of RGB values, ranging from 0-255.
    (255, 255, 255), (128, 128, 128), (64, 64, 64), (0, 0, 0)]

"""Windows are created by inheriting from the Window class.
They are then added to a PGUI Program using it's window decorator."""
@example_program.window(window_id="example")
class ExampleWindow(Window):

    """The title, width and height of the window are class attributes.
    This means they're defined in the class rather than any class function."""
    title = "First Window!"
    width = 240
    height = 100

    """Start is called when the window is run for the first time.
    This is a good place to create your elements!"""
    def start(self):

        """Elements are added to windows by creating an instance of them.
        There's a lot to choose from, but if you want to create your own,
        it's as easy as inheriting from the UIElement class."""
        self.my_button = elements.Button(
            ##Each element has reference to its parent window.
            window=self,
            ##Most elements have x, y, width, and height for drawing them.
            x=20, y=20, width=200, height=50, 
            ##These are element specific params.
            text="start", text_size=40, 
            action=self.open_window, 
            arguments=("example2")
        )


"""If the popup parameter of the window decorator is set to false, closing the 
window does not quit the program."""
@example_program.window(window_id="example2", popup=False)
class AnotherExampleWindow(Window):
    title = "Second Window!"
    width = 240
    height = 150

    def start(self):
        ##If I don't put something here the start docstring gets overwritten.
        THE_ANSWER = 42

        """The `loadify` function is a way of loading images to pygame much 
        faster than the normal pg.image.load(). It takes a path from the 
        current directory to the image file, as well as optional scale and 
        colourkey. Colourkey makes all parts of the image which are the keyed 
        colour transparent. In this case, all white parts of the image."""
        self.my_image = loadify("ExampleImage.png", (50, 50), (255, 255, 255))

        """Instance variables can be created within the window's scope. To 
        make a "global" variable, use `self.parent.my_variable`."""
        self.pos, self.target = 0, 0

        self.my_button = elements.Button(
            window=self, 
            x=20, y=20, 
            width=200, height=50, 
            text="Disco!", text_size=40,
        )

    """Update is called once per frame for updating things."""
    def update(self):
        if self.my_button.clicked():
            self.parent.colours[0] = tuple(randint(0, 255) for i in range(3))

        if self.pos in range(self.target-5, self.target+5):
            self.target = randint(0, self.width-50)
        
        self.pos += (-5 if self.pos > self.target else 5)

    """Draw is called once per frame for drawing things."""
    def draw(self):
        self.screen.blit(self.my_image, (self.pos, 100))


if __name__ == "__main__":
    example_program.run()