"""Adds UI elements based on pygame."""

from .program import Program
from .window import Window
from .spacing import space_elements
from .uielement import UIElement
from .UIElements.image import loadify